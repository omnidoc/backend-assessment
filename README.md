# <img src="resources/omnidoc-logo.png" alt="OmniDoc Logo" width="120px"/> Backend developer assessment 

![OmniDoc Logo](resources/logo.jpeg)

## What are we looking for?

We are looking for members who are interested in being part of an agile, progressive, proactive team with new ideas.
We seek to use technology as an ally to reachr our goals as well as to offer customers a better user experience as well as delivering added value in our products.

## Assessment:

### Cash Machine

Create a solution to represent a User with the follow use cases:

- I as a *Client* can sing up as a new *User*, when I get registered my debit account most have an initial value greater than $1000, the credit account should be initialized witn an initial value take it from debit account.

- I as a *User* wants to be able to withdraw just the available amount from my debit account, in other case the system should return an exception.

- I as a *User* wants to be able to increase my savings.

- I as a *User* wants to be able to withdraw just the available amount from credit account, in this case there are a 5% fee for withdraw, in other case the system should return an exception

- I as a *User* wants pay for credit account


### Requirement

- Add file README.md with instructions to run the project
- Languajes use PHP, Java, JavaScript, TypeScript, or Python to solve the problem
- You can use any SQL DBM as MySQL or Postgres
- You can use any modern Framework as Symfony, Laravel, NestJs, Express, Django, Flask, Springboot)

### Evaluation

- We´re goint to evaluate best practices, standards, logic, any effort to write clean code, maintainable, readeable etc.

Enjoy Coding!